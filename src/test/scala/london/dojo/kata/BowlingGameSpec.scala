package london.dojo.kata

import org.scalatest.{Matchers, FlatSpec, FeatureSpec}


case class Game(rolls: List[Int]) {
  //most recent roll first in rolls list
 // def roll(pins: Int): Game = new Game(pins :: rolls)

  def score(): Int = {
    val listofFrames = rollsIntoPairs(rolls, 1)
    listofFrames.map(_.score).sum
  }

  def firstFrameScore(frames: List[Frame]): Int = {
    frames.head.score
  }

  def rollsIntoPairs(rolls: List[Int], frameNumber: Int): List[Frame] =
    if (frameNumber < 10)
      rolls match {
        case 10 :: restOfRolls => Frame(List(10), restOfRolls) :: rollsIntoPairs(restOfRolls, frameNumber + 1)
        case roll1 :: roll2 :: restOfRolls => Frame(List(roll1, roll2), restOfRolls) :: rollsIntoPairs(restOfRolls, frameNumber + 1)
        case _ => Nil
      }
      else
        List(Frame(rolls, Nil))
}

case class Frame(rolls: List[Int], followingRolls: List[Int]) {
  def score = rolls.sum + bonus

  def bonus = if (isSpare) followingRolls.head else if (isStrike) followingRolls.take(2).sum else 0

  def isSpare = !isStrike && rolls.sum == 10

  def isStrike = rolls.head == 10
}

class BowlingGameSpec extends FlatSpec with Matchers {
  def runGame(rolls: List[Int]): Int = {
    new Game(rolls).score
  }

  "A game" should "score 0 if never hits any pins" in {
    runGame((1 to 20).map(_ => 0).toList) should be (0)
  }

  it should "score 20 if it hits 1 every roll" in {
    runGame((1 to 20).map(_ => 1).toList) should be (20)
  }

  it should "score 29 if it hits spare and then 1 every remaining roll" in {
    runGame(5::5 :: (1 to 18).map(_ => 1).toList ) should be (29)
  }

  it should "score 30 if it hits strike and then 1 every remaining roll" in {
    runGame(10 :: (1 to 18).map(_ => 1).toList ) should be (30)
  }

  it should "score 49 if it hits 2 strikes and then 1 every remaining roll" in {
    runGame(10 :: 10 :: (1 to 16).map(_ => 1).toList) should be(49)
  }

  it should "score 30 if it hits 18 1's and then a strike and 2 more 1's" in {
    runGame((1 to 18).map(_ => 1).toList ++ List(10, 1, 1)) should be(30)
  }

  it should "score 28 if it hits 18 1's and then a strike and 2 gutterballs" in {
    runGame((1 to 18).map(_ => 1).toList ++ List(10, 0, 0)) should be(28)
  }

  it should "score 30 if it hits 18 1's and then a spare and a gutterball" in {
    runGame((1 to 18).map(_ => 1).toList ++ List(5, 5, 0)) should be(28)
  }

}
