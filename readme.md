The template SBT project for Dojo Kata and Scala Bombs!

This project also contains all of the previous problems that were expressed as a failing test.

To check this project out:

* Download [Atlassian SourceTree](https://www.sourcetreeapp.com) and install
* Open SourceTree and follow the prompts to configure your Bitbucket username and password
* Click the `Clone / New` button
* Paste the URL `https://bitbucket.org/ShaolinSarg/dojo-kata.git` into the `Source Path / URL` box
* Click `Clone`

If you're nominated, copy the Kata.scala / KataSpec.scala files and name them something more appropriate to the task in hand.